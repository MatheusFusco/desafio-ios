//
//  Repository.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "Repository.h"

@implementation Repository
//-(id)init
//{
//    self = [super init];
//    if (self) {
//        
//    }
//    return self;
//}
+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{
             @"name" : @"full_name",
             @"descr" : @"description",
             @"numberOfStars" : @"watchers_count",
             @"numberOfForks" : @"forks_count",
             @"openIssues" : @"open_issues_count",
             @"userName" : @"owner.login",
             @"userPhoto" : @"owner.avatar_url"
             };
}
-(instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    return self;
}
@end
