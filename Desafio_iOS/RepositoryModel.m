//
//  HomeModel.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "RepositoryModel.h"
#import "ConnectionService.h"
#import "Repository.h"

@implementation RepositoryModel

-(void)downloadRepositories
{
    ConnectionService * service = [[ConnectionService alloc] init];
    
    [service getRepository:^(NSError *error, BOOL success, NSArray *repository) {
        if (success) {
            NSMutableArray * repoArray = [[NSMutableArray alloc] init];
            
            for (int i = 0; i < repository.count; i++) {
                NSDictionary * dict = [[NSDictionary alloc] init];
                dict = [repository objectAtIndex:i];
                
                Repository * rp = [MTLJSONAdapter modelOfClass:[Repository class] fromJSONDictionary:dict error:&error];
                [repoArray addObject:rp];
            }
            [self.delegate updateTableViewWithRepositories:repoArray];
        }
    }];
}

@end
