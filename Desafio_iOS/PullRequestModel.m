//
//  PullRequestModel.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "PullRequestModel.h"
#import "ConnectionService.h"
#import "PullRequest.h"

@implementation PullRequestModel

-(void)downloadPullRequest:(NSString*)fromRepository
{
    ConnectionService * service = [[ConnectionService alloc] init];
    [service getPullRequest:fromRepository callback:^(NSError *error, BOOL success, NSArray *pr) {
        if (success) {
            NSMutableArray * prArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < pr.count; i++) {
                NSDictionary * dict = [[NSDictionary alloc] init];
                dict = [pr objectAtIndex:i];

                PullRequest * pr = [MTLJSONAdapter modelOfClass:[PullRequest class] fromJSONDictionary:dict error:&error];
                [prArray addObject:pr];
            }
            [self.delegate updateTableViewWithPullRequests:prArray];
        }
    }];
}
@end
