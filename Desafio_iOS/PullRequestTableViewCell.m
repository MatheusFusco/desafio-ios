//
//  PullRequestTableViewCell.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "PullRequestTableViewCell.h"
@interface PullRequestTableViewCell()

@end

@implementation PullRequestTableViewCell
@synthesize userPhoto, usernameLabel, titleLabel, bodyLabel, prDate;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

-(void)setPullRequest:(PullRequest *)pullRequest
{
    _pullRequest = pullRequest;
    self.titleLabel.text = _pullRequest.title;
    self.bodyLabel.text = _pullRequest.body;
    
    self.usernameLabel.text = _pullRequest.userName;

    self.prDate.text = [self convertDate:_pullRequest.date];
    
    
    [self.userPhoto sd_setImageWithURL:[NSURL URLWithString:_pullRequest.userPhoto] placeholderImage:[UIImage imageNamed:@"user_photo.png"]];
    self.url = _pullRequest.prURL;
}

- (void)awakeFromNib {
    // Initialization code
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(NSString *) convertDate :(NSString *)dateToConvert
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ss'Z'"];
    NSDate *prevday = [formatter dateFromString:dateToConvert];

    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    [formatter2 setDateFormat:@"MMM, dd/YYYY - HH:mm:ss"];
    NSString * str = [formatter2 stringFromDate:prevday];

    return str;
}

@end
