//
//  RepositoryTableViewController.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RepositoryTableViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray * repositories;
@end
