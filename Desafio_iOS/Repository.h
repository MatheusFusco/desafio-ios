//
//  Repository.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"

@interface Repository : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * descr;
@property (nonatomic, assign) NSInteger  numberOfStars;
@property (nonatomic, assign) NSInteger  numberOfForks;
@property (nonatomic, assign) NSInteger  openIssues;
@property (nonatomic, strong) NSString * userName;
@property (nonatomic, strong) NSString * userPhoto;

@end
