//
//  PullRequest.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "PullRequest.h"

@implementation PullRequest
//-(id)init
//{
//    self = [super init];
//    if (self) {
//        
//    }
//    return self;
//}
+(NSDictionary *) JSONKeyPathsByPropertyKey
{
    return @{
             @"title" : @"title",
             @"date" : @"created_at",
             @"body" : @"body",
             @"userName" : @"user.login",
             @"userPhoto" : @"user.avatar_url",
             @"prURL" : @"html_url"
             };
}

-(instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error
{
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    return self;
}

@end
