//
//  ConnectionService.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 05/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionService : NSObject
-(void)getRepository:(void (^)(NSError *error, BOOL success, NSArray * repository))callback;
-(void)getPullRequest:(NSString*)fromRepository callback:(void (^)(NSError *error, BOOL success, NSArray * pr))callback;
@end
