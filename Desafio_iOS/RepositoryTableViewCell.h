//
//  RepositoryTableViewCell.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Repository.h"
#import "UIImageView+WebCache.h"

@interface RepositoryTableViewCell : UITableViewCell
@property (nonatomic, strong) Repository * repository;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfStarsLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberOfForksLabel;
@property (weak, nonatomic) IBOutlet UILabel *repositoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *repositoryDescriptionLabel;
@end
