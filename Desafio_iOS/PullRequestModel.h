//
//  PullRequestModel.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PullRequestModelDelegate;
@interface PullRequestModel : NSObject
-(void)downloadPullRequest:(NSString*)fromRepository;
@property (nonatomic, weak) id <PullRequestModelDelegate> delegate;
@end
@protocol PullRequestModelDelegate <NSObject>
-(void)updateTableViewWithPullRequests:(NSMutableArray *)prs;
@end