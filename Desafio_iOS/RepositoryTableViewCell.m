//
//  RepositoryTableViewCell.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "RepositoryTableViewCell.h"
@interface RepositoryTableViewCell()

@end

@implementation RepositoryTableViewCell
@synthesize repositoryNameLabel, repositoryDescriptionLabel, userPhoto, usernameLabel, numberOfForksLabel, numberOfStarsLabel;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

-(void)setRepository:(Repository *)repository
{
    //_repository = repository;
    self.repositoryNameLabel.text = repository.name;
    self.repositoryDescriptionLabel.text = repository.descr;
    
    self.numberOfForksLabel.text = [NSString stringWithFormat:@"%ld", (long)repository.numberOfForks];
    self.numberOfStarsLabel.text = [NSString stringWithFormat:@"%ld", (long)repository.numberOfStars];
    
    self.usernameLabel.text = repository.userName;
    
    [self.userPhoto sd_setImageWithURL:[NSURL URLWithString:repository.userPhoto] placeholderImage:[UIImage imageNamed:@"user_photo.png"]];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
