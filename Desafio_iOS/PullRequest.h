//
//  PullRequest.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTLModel.h"
#import "MTLJSONAdapter.h"
#import "MTLValueTransformer.h"

@interface PullRequest : MTLModel <MTLJSONSerializing>
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * date;
@property (nonatomic, strong) NSString * body;
@property (nonatomic, strong) NSString * userName;
@property (nonatomic, strong) NSString * userPhoto;
@property (nonatomic, strong) NSString * prURL;
@end
