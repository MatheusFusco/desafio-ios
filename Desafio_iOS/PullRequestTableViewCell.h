//
//  PullRequestTableViewCell.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRequest.h"
#import "UIImageView+WebCache.h"

@interface PullRequestTableViewCell : UITableViewCell
@property (nonatomic, strong) PullRequest * pullRequest;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;
@property (weak, nonatomic) IBOutlet UILabel *prDate;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userPhoto;
@property (nonatomic, strong) NSString * url;

@end
