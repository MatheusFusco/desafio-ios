//
//  PullRequestTableViewController.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 03/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullRequestTableViewController : UITableViewController
@property (nonatomic, strong) NSMutableArray * pullRequests;
@property (nonatomic, strong) NSString * repository;
@end
