//
//  HomeModel.h
//  Desafio_iOS
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <Realm/Realm.h>
@protocol RepositoryModelDelegate;

@interface RepositoryModel : NSObject
-(void)downloadRepositories;
@property (nonatomic, weak) id<RepositoryModelDelegate> delegate;
@end
@protocol RepositoryModelDelegate <NSObject>
-(void)updateTableViewWithRepositories:(NSMutableArray *)repos;
@end
