//
//  ConnectionService.m
//  Desafio_iOS
//
//  Created by Matheus Fusco on 05/05/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import "ConnectionService.h"
#import "AFURLRequestSerialization.h"
#import "AFURLSessionManager.h"
#import "Repository.h"
#import "PullRequest.h"

@implementation ConnectionService{
    NSArray * array;
}

-(void)getRepository:(void (^)(NSError *error, BOOL success, NSArray * repository))callback
{
    array = [[NSArray alloc] init];;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString * URLString = @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";
    NSURL *URL = [NSURL URLWithString:URLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            array = [responseObject objectForKey:@"items"];
            //NSLog(@"response object: %@", array);
            callback(error, YES, array);
        }
    }];
    [dataTask resume];
}

-(void)getPullRequest:(NSString*)fromRepository callback:(void (^)(NSError *error, BOOL success, NSArray * pr))callback
{
    array = [[NSArray alloc] init];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString * URLString = [NSString stringWithFormat: @"https://api.github.com/repos/%@/pulls", fromRepository];
    NSURL *URL = [NSURL URLWithString:URLString];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            array = responseObject;
            //NSLog(@"response object: %@", array);
            callback(error, YES, array);
        }
    }];
    [dataTask resume];
}
@end















//-(void)getDataFrom:(NSString *)urlString callback:(void (^)(NSError *error, BOOL success, id responseArray))callback
//{
//    array = [[NSArray alloc] init];
//
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//
//    NSURL *URL = [NSURL URLWithString:urlString];
//    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
//
//    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//        if (error) {
//            NSLog(@"Error: %@", error);
//        } else {
//            array = responseObject ;
//            callback(error, YES, array);
//        }
//    }];
//    [dataTask resume];
//}
//
//-(NSMutableArray *)getRepository
//{
//    NSMutableArray * repoArray = [[NSMutableArray alloc] init];
//    NSString * urlString = @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1";
//    [self getDataFrom:urlString callback:^(NSError *error, BOOL success, id responseArray) {
//        if (success)
//        {
//            NSArray * arrayReturned = [[NSArray alloc] init];
//            arrayReturned = [responseArray objectForKey:@"items"];
//            for (int i = 0; i < arrayReturned.count; i++) {
//                NSDictionary * dict = [[NSDictionary alloc] init];
//                dict = [arrayReturned objectAtIndex:i];
//
//                Repository * repo = [[Repository alloc] init];
//
//                repo.name = [dict objectForKey:@"full_name"];
//                repo.descr = [dict objectForKey:@"description"];
//                repo.numberOfStars = [[dict objectForKey:@"watchers_count"] integerValue];
//                repo.numberOfForks = [[dict objectForKey:@"forks_count"] integerValue];
//                repo.openIssues = [[dict objectForKey:@"open_issues_count"] integerValue];
//
//                repo.userName = [[dict objectForKey:@"owner"] objectForKey:@"login"];
//                repo.userPhoto = [[dict objectForKey:@"owner"] objectForKey:@"avatar_url"];
//
//                [repoArray addObject:repo];
//            }
//        }
//    }];
//    return repoArray;
//}

