//
//  Desafio_iOSTests.m
//  Desafio_iOSTests
//
//  Created by Matheus Fusco on 29/04/16.
//  Copyright © 2016 ConcreteSolutions. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "RepositoryTableViewController.h"

#import "PullRequestTableViewController.h"

#import "ConnectionService.h"

@interface Desafio_iOSTests : XCTestCase{
    RepositoryTableViewController * repoVC;
    PullRequestTableViewController * prVC;
}

@end

@implementation Desafio_iOSTests 

- (void)setUp {
    [super setUp];
    repoVC = [[RepositoryTableViewController alloc] init];
    prVC = [[PullRequestTableViewController alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

#pragma mark - RepositoryTableView Tests
-(void)testRepositoryViewController
{
    XCTAssertNotNil(repoVC, @"RepositoryViewController should not be nil");
}

- (void)testThatRepoViewConformsToUITableViewDataSource
{
    XCTAssertTrue([repoVC conformsToProtocol:@protocol(UITableViewDataSource) ], @"View does not conform to UITableView datasource protocol");
}

- (void)testThatRepoTableViewHasDataSource
{
    XCTAssertNotNil(repoVC.tableView.dataSource, @"Table datasource cannot be nil");
}

- (void)testThatRepoViewConformsToUITableViewDelegate
{
    XCTAssertTrue([repoVC conformsToProtocol:@protocol(UITableViewDelegate) ], @"View does not conform to UITableView delegate protocol");
}

- (void)testRepoTableViewIsConnectedToDelegate
{
    XCTAssertNotNil(repoVC.tableView.delegate, @"Table delegate cannot be nil");
}





#pragma mark - PullRequestTableView Tests
-(void)testPullRequestView
{
    XCTAssertNotNil(prVC, @"PullRequestTableViewController should not be nil");
}

- (void)testThatPRViewConformsToUITableViewDataSource
{
    XCTAssertTrue([prVC conformsToProtocol:@protocol(UITableViewDataSource) ], @"View does not conform to UITableView datasource protocol");
}

- (void)testThatPRTableViewHasDataSource
{
    XCTAssertNotNil(prVC.tableView.dataSource, @"Table datasource cannot be nil");
}

- (void)testThatPRViewConformsToUITableViewDelegate
{
    XCTAssertTrue([prVC conformsToProtocol:@protocol(UITableViewDelegate) ], @"View does not conform to UITableView delegate protocol");
}

- (void)testPRTableViewIsConnectedToDelegate
{
    XCTAssertNotNil(prVC.tableView.delegate, @"Table delegate cannot be nil");
}




#pragma mark - ConnectionService Tests
-(void)testConnectionServiceForRepository
{
    ConnectionService * service = [[ConnectionService alloc] init];
    
    [self measureBlock:^{
        [service getRepository:^(NSError *error, BOOL success, NSArray * repository) {
            XCTAssertTrue(success, @"success should be true");
            XCTAssertNotNil(repository, @"repository list should not be nil");
            XCTAssert([repository isKindOfClass:[NSArray class]], @"repository list should be NSArray");
            XCTAssert([[repository objectAtIndex:1] isKindOfClass:[NSDictionary class]], @"repository list components should be NSDictionary");
        }];
    }];
}

-(void)testConnectionServiceForPullRequest
{
    ConnectionService * service = [[ConnectionService alloc] init];
    
    [self measureBlock:^{
        [service getPullRequest:@"facebook/react-native" callback:^(NSError *error, BOOL success, NSArray * pr) {
            XCTAssertTrue(success, @"success should be true");
            XCTAssertNotNil(pr, @"pullrequest list should not be nil");
            XCTAssert([pr isKindOfClass:[NSArray class]], @"pr list should be NSArray");
            XCTAssert([[pr objectAtIndex:1] isKindOfClass:[NSDictionary class]], @"pr list components should be NSDictionary");
        }];
    }];
}

@end
